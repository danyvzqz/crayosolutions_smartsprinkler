import time
import sys
import signal
import os
import os.path
import logging
import traceback 
sys.path.append('/root/Shared/')
from PkgComm import *

#Constants Definitions
ID = '0000000002'

#Enable the Logger
logging.basicConfig(
    format='%(asctime)s, %(message)s',
    filename=time.strftime("%Y_%m_%d_%H%M%SConnection.log", time.localtime()),
    level=logging.INFO)
    
logging.getLogger().addHandler(logging.StreamHandler())

try:
    #Move to AP Mpde 
    logging.info("Switching to AP Mode")
    #os.system(uci set wireless.sta.disabled=1)
    #os.system(uci commit)
    #os.system(wifi)
    
    #Since this is the sensor, initialize the producer scheme
    logging.info("Opening Communication Ports")
    os.system('iptables -A INPUT -j ACCEPT -p tcp --dport ' + str(PORT))
    os.system('iptables -A OUTPUT -j ACCEPT -p tcp --sport ' + str(PORT))
    os.system('iptables -A OUTPUT -j ACCEPT -p tcp --dport ' + str(PORT))
    waterCtrlSocket = DataConsumer(ID, 'Water Control')
    
    #Add the GET_GENERIC_DATA_MSG
    logging.info("Opening Communication Ports")
    waterCtrlSocket.addMessage(MessageHandler(MsgType = GET_GENERIC_DATA_MSG, Validate = True))
    
    #Add the Toggle Network Message
    toggleMessage = MessageHandler(-1, False)
    logging.info("Adding Toggle Network Message")
    toggleMessage.addProducerStep({'type': SEND_TX, 'data': "Toggling"})
    toggleMessage.addConsumerStep(RX_STEP)
    waterCtrlSocket.addMessage(toggleMessage)
    
    logging.info("Interrupt scheme")
    def signal_handler(sig, frame):
        print('You pressed Ctrl+C')
        waterCtrlSocket.close()
        sys.exit(0)
    
    signal.signal(signal.SIGINT, signal_handler)
    
    #Main
    #Request the next Messages in the next order:
    #1. Suscribe(SUSCRIBE_MSG)
    #2. Query Info(QUERY_MSG)
    #3. Get Analog Data (GET_GENERIC_DATA_MSG):
    for i in [SUSCRIBE_MSG, QUERY_MSG, GET_GENERIC_DATA_MSG]:
        if i == SUSCRIBE_MSG:
            logging.info("Suscribing...")
        elif i == QUERY_MSG:
            logging.info("Querying...")
        elif i == GET_GENERIC_DATA_MSG:
            logging.info("Getting Analog Data...")
            
        logging.info("Connecting")
        connectionResult = waterCtrlSocket.connect()
        if not connectionResult:
        #We are connected!!! 
            logging.info("Connected")
            msg = waterCtrlSocket.request(i)
            if msg['Error'] != 0:
                logging.error("Request Error: " + str(msg['Error']))
                break
            else:
                logging.info("Values from Conversation: " + str(msg['RX']))
        else:
            logging.info("Connection failed: " + str(connectionResult))
            break

    logging.info("Connection Closed. Bye!")
        
    #Cleanup
    waterCtrlSocket.close()
except:
    logging.error("Error Occured: " + str(traceback.format_exc()))
    if 'waterCtrlSocket' in locals()  or 'waterCtrlSocket' in globals():
        waterCtrlSocket.close()
    raise