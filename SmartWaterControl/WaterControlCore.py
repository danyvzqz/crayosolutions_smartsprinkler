import mraa
import time
import signal
import sys
sys.path.append('/root/Shared/')
from PkgWaterControl import *

#logging.INFO logging.DEBUG logging.ERROR
DEBUG_LEVEL = logging.INFO
DEBUG_TO_FILE = True

# Variables definitions
DayTime_now = {'Weekday': 'Monday', 'Hours': '0', 'Minutes': '0'} # Dictionary to store the current weekday and time
WateringSettings = {} # Dictionary to store watering settings

# Constants definitions
ID = '0000000002'
SolenoidPin = mraa.Gpio(18) # Initialize GPIO18 (P25 on LinkIt Smart 7688 board)

#Initializations
# Enable the Logger
if DEBUG_TO_FILE:
    logging.basicConfig(
        format='%(asctime)s, %(message)s',
        filename=time.strftime("%Y_%m_%d_%H%M%SCore.log", time.localtime()),
        level=DEBUG_LEVEL)
else:
    logging.basicConfig(
        format='%(asctime)s, %(message)s',
        level=DEBUG_LEVEL)
    

logging.getLogger().addHandler(logging.StreamHandler())

SolenoidPin.dir(mraa.DIR_OUT) # Set the solenoid pin as OUTPUT

#Initialize Communication
initializeComm(ID)

#Interrupt scheme
def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Main system loop
while True:
    WateringSettings = getWateringSettings() # Obtain watering settings and store them in a dictionary
    timesPerWeek = int(WateringSettings['Times']) # Store total number of times to water the lawn per week
    logging.info("times per week: %d" % timesPerWeek)
    DayTime_now = getDayTime() # Obtain the current weekday and time and store them in a list
    today = DayTime_now['Weekday'] # Store today weekday
    logging.info("today: %s" % today)
    if (today in WateringSettings): # If today is a watering day then
        logging.info("Today is Watering Day!")
        startHour   = int(WateringSettings['Hour1']) # Obtain start hour to water the lawn from watering settings list 
        startMinute = int(WateringSettings['Min1']) # Obtain start minute to water the lawn from watering settings list 
        endHour     = int(WateringSettings['Hour2']) # Obtain end hour to water the lawn from watering settings list 
        endMinute   = int(WateringSettings['Min2']) # Obtain end minute to water the lawn from watering settings list 
        currentMinutes = (int(DayTime_now['Hours']) * 60) + int(DayTime_now['Minutes'])
        logging.info("Current time is " + str(DayTime_now['Hours']) + ":" + str(DayTime_now['Minutes']) + ")")
        if (currentMinutes >= ((startHour * 60) + startMinute)) and \
           (currentMinutes < ((endHour * 60) + endMinute)): # If now is time (hour and minute) to water the lawn then
            logging.info("It's time to water!")
            zipCode = WateringSettings['Zip'] # Obtain zip code from watering settings list
            if precipitation(zipCode) == False: # If now there is no precipitation (any kind) then
                logging.info("It's not raining!")
                if isItDry(): # If the lawn is dry then
                    logging.info("And it's dry. Watering lawn!!!!")
                    MaxWateringTime = min(30, (((endHour - startHour) * 60) + endMinute - startMinute)) # Calculate maximum watering time (in minutes) acording the total number of times to water the lawn per week 
                    wateringLawn(SolenoidPin, MaxWateringTime) # Water the lawn
                else:
                    logging.info("It's not dry, skip")
            else:
                logging.info("It's raining or it's going to rain, skip")
        else:
            logging.info("Not quite watering time, skip (Water Times " + str(startHour) \
                  + ":" + str(startMinute) + " to " + str(endHour) \
                  + ":" + str(endMinute) + ")")
    else:
        logging.info("Not a watering day, skip")
                
    time.sleep(60)