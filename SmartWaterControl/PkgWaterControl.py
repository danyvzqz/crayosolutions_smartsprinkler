import time
import urllib2
import json
import sys
import signal
import os
import os.path
import logging
import traceback 

sys.path.append('/root/Shared/')
from PkgComm import *

# This package contains the necessary functions to drive the actuators

# Constants definitions
threshold = 70 # percent

def initializeComm(ID):
    try:
        global waterCtrlSocket
        waterCtrlSocket = DataConsumer(ID, 'Water Control')
        # Add the GET_GENERIC_DATA_MSG
        logging.info("Opening Communication Ports")
        waterCtrlSocket.addMessage(MessageHandler(MsgType = GET_GENERIC_DATA_MSG, Validate = True))
        logging.info("Connecting")
        connectionResult = waterCtrlSocket.connect()
        if not connectionResult:
            logging.info("Connected")
            logging.info("Suscribing...")
            msg = waterCtrlSocket.request(SUSCRIBE_MSG)
            
            if msg['Error'] != 0:
                logging.error("Request Error: " + str(msg['Error']))
                raise Exception("Request Error: " + str(msg['Error']))
            else:
                logging.info("Values from Conversation: " + str(msg['RX']))
        else:
            logging.info("Connection failed: " + str(connectionResult))
            raise Exception("Connection failed: " + str(connectionResult))
    except:
        logging.error("Error Occured: " + str(traceback.format_exc()))
        if 'waterCtrlSocket' in locals()  or 'waterCtrlSocket' in globals():
            waterCtrlSocket.close()
        raise
    return waterCtrlSocket

def isItDry():
    dry = True # Default value is dry
    logging.info("Connecting")
    connectionResult = waterCtrlSocket.connect()
    if not connectionResult:
        logging.info("Connected")
        logging.info("Getting Analog Data...")
        msg = waterCtrlSocket.request(GET_GENERIC_DATA_MSG)
            
        if msg['Error'] != 0:
            logging.error("Request Error: " + str(msg['Error']))
            waterCtrlSocket.close()
            raise Exception("Request Error: " + str(msg['Error']))
        else:
            logging.info("Values from Conversation: " + str(msg['RX']))
            sensorValue = int(msg['RX'][0][0])
            dry = sensorValue <= threshold
    else:
        logging.info("Connection failed: " + str(connectionResult))
        waterCtrlSocket.close()
        raise Exception("Connection failed: " + str(connectionResult))
    return dry

# Function that stores the current weekday and time in a dictionary received as a 
# parameter
def getDayTime(): 
    DayTime_now = []
    DayTime_now['Weekday'] = time.strftime("%A") # Get and store current weekday in the list 
    DayTime_now['Hours'] = time.strftime("%H") # Get and store current hours in the list
    DayTime_now['Minutes'] = time.strftime("%M") # Get and store current minutes in the list
    return DayTime_now

# Function that returns (True or False) if any kind of precipitation is 
# occurring now in the zip code received as a parameter
def precipitation(zipCode): 
    weatherApi_key = '26e918ad7182a1bd' # Weather Underground API key
    weatherApi_url = 'http://api.wunderground.com/api/' + weatherApi_key + '/geolookup/conditions/q/IA/' + zipCode + '.json' # Weather Underground API request url 
    f = urllib2.urlopen(weatherApi_url) # Request to Weather Underground the current conditions in the indicated zip code
    json_string = f.read() # Get the JSON data from the request
    parsed_json = json.loads(json_string) # Decode JSON data
    weather_f = parsed_json['current_observation']['weather'] # Obtain current weather condition phrase
    if (weather_f == 'Drizzle') or (weather_f == 'Thunderstorms with Hail') or \
       (weather_f == 'Snow') or (weather_f == 'Snow Grains') or \
       (weather_f == 'Ice Crystals') or (weather_f == 'Ice Pellets') or \
       (weather_f == 'Hail') or (weather_f == 'Low Drifting Snow') or \
       (weather_f == 'Blowing Snow') or (weather_f == 'Rain Mist') or \
       (weather_f == 'Freezing Drizzle') or (weather_f == 'Snow Showers') or \
       (weather_f == 'Snow Blowing Snow Mist') or (weather_f == 'Squalls') or \
       (weather_f == 'Ice Pellet Showers') or (weather_f == 'Hail Showers') or \
       (weather_f == 'Small Hail Showers') or (weather_f == 'Thunderstorm') or \
       (weather_f == 'Thunderstorms and Rain') or (weather_f == 'Small Hail') or \
       (weather_f == 'Thunderstorms and Snow') or (weather_f == 'Rain Showers') or \
       (weather_f == 'Unknown Precipitation') or (weather_f == 'Freezing Rain') or \
       (weather_f == 'Thunderstorms and Ice Pellets') or (weather_f == 'Rain')  or \
       (weather_f == 'Thunderstorms with Small Hail'): # If the current weather condition phrase is a precipitation then 
        precip = True
    else:
        precip = False
    f.close()
    return precip

# Function that reads the watering settings (custom or default) written in a 
# text file, and stores them in the dictionary received as a parameter
def getWateringSettings(WateringSettings): 
    WateringSettings = []
    with open("/usr/share/htdocs/WateringSettings.txt", "r") as settingsFile: # Open the text file that has the watering settings
        for line in settingsFile:
            splitLine = line.split()
            WateringSettings[splitLine[0]] = splitLine[1].rstrip() # Store each line of the file in the dictionary
    settingsFile.close()
    return WateringSettings

# Function that performs the lawn watering
def wateringLawn(SolenoidPin, MaxWateringTime): 
    WateringTime = 0 # Initialize watering timer in 0 minutes
    while isItDry() and WateringTime < MaxWateringTime: # Water the lawn while still is dry and watering timer is less than the maximum watering time
        t1 = time.time() # Get current time
        SolenoidPin.write(1) # Set the solenoid pin state to HIGH (open valve to let the water flow)
        time.sleep(60)
        WateringTime += 1
    SolenoidPin.write(0) # Set the solenoid pin state to LOW (close the valve to stop water flow)
    return