import socket
import os
import re
import time

#This package contains the necessary classes to communicate the sensor with the
#actuators. The sensors are typically producers and the actuators are consumers.
#The Data Consumer needs to start the conversation, therefore DataProducers will
#be consantly listening for Commands. A typical  communication will be like this:
#      Consumer                                 Producer
#  RequestConnection              --> 
#                                 <--        Accept Connection
#  RequestCmdAccess(ID, CommType) -->
#                                 <--       Ack/Deny Connection (Close if Deny)
#  Command(Var)                   -->
#                                 <--         Command Response
#                                 ...
#  EOM (Close)                    <->              (Close)
#For now the only two commands available are Suscribe, Query and ReadHumidity. 
#Later On the suscribe will be able to enable adding more commands during run 
#time


#Constants
#Since the communication will occur while the Producer is on AP Mode, the 
#address and Port can be constants
#ADDRESS = '192.168.100.1'
ADDRESS = '192.168.0.112'
PORT    = 1607
BUFSIZE = 1024

#MessageHandle Constants
SUSCRIBE_MSG         = 0
GET_GENERIC_DATA_MSG = 1
QUERY_MSG            = 2

WAIT_RX = 100
SEND_TX = 101

#Character constants
SEPARATOR = '\x1F'
NAK       = '\x15'
ACK       = '\x06'
EOT       = '\x04'

RX_STEP = {'type': WAIT_RX, 'data': None}

#Errors
TIMEOUT = -1


#Classes
class MessageHandler:
    def __init__(self, MsgType, Validate = True, StepsConsumer = [], StepsProducer = []):
        """
          The constructor creates a MessageHandler to be used by the Data 
          Producer and consumer classes. 
          @param Type: Type of the Message, although new messages can be created 
          on the fly, only messages type defined above should be used
          @param StepsConsumer: (optional) List of steps to be performed by the 
          Consumer. If not defined it will be empty and AddConsumerStep should 
          be used
          @param StepsProducer: (optional) List of steps to be performed by the 
          Producer. If not defined it will be empty and AddProducerStep should 
          be used
        """
        self.MsgType       = MsgType
        self.consumerSteps = []
        self.producerSteps = []
        self.validate      = Validate

        #There are two special cases, defined by Suscribe and Get Humidity MSG
        if MsgType == SUSCRIBE_MSG:
            #For the Suscribe the consumer will send
            self.consumerSteps.append(RX_STEP)
            self.producerSteps.append({'type': SEND_TX, 'data': "\x06"})
        elif self.MsgType == GET_GENERIC_DATA_MSG:
            #For the Generic Data the consumer will only wait for the Sensor Data
            #that the Producer will give. Note that since the Data varies, we 
            #just define it as NotImplemented and hope that it is passed later 
            #on when dealing with the Message.
            self.consumerSteps.append(RX_STEP)
            self.producerSteps.append({'type': SEND_TX, 'data': None})
        else:
            #Add the steps from the parameters
            self.consumerSteps.extend(StepsConsumer)
            self.producerSteps.extend(StepsProducer)
            
        
    def __addStep(self, Step, toProducer = True):
        """
          Adds a step to the list of steps specified by the toProducer parameter
          @param Step: Step to be added
          @param toProducer: True inidicates if the list should be added to the 
            Producer List, False indicates that it should be added to the 
            Consumer list
        """
        if toProducer:
            self.producerSteps.append(Step)
        else:
            self.consumerSteps.append(Step)
    
    def addProducerStep(self, Step):
        """
          Adds a step to the producer steps list
          @param Step: Step to be added
        """
        self.__addStep(Step, toProducer = True)
        
    def addConsumerStep(self, Step):
        """
          Adds a step to the consumer steps list
          @param Step: Step to be added
        """
        self.__addStep(Step, toProducer = False)
    
    def makeQueryMsg (self, ID, Description):
        """
          Using this function will make the current Steps to be overwritten. 
          @param ID: Unique identifier of the device
          @param DataType: What type of Data is being produced by the device
        """
        self.MsgType       = QUERY_MSG
        self.consumerSteps = []
        self.producerSteps = []
        self.addProducerStep({'type': SEND_TX, 'data': str(ID)}) 
        self.addConsumerStep(RX_STEP)
        self.addProducerStep({'type': SEND_TX, 'data': str(Description)}) 
        self.addConsumerStep(RX_STEP)
        
    
class DataAgent(object):
    def __init__(self, ID, Description, Address=ADDRESS, Port = PORT, \
    BufSize=BUFSIZE):
        """
          The constructor instantiates the Data Agent
          @param ID: Unique Identification number of the device
          @param Description: What type of Data is being produced by the device
          @param Address: Address to open the connection (Default to ADDRESS)
          @param Socket: Socket for the connection (Default to SOCKET)
          @param BufSize: Max Buffer Size for the messages(Default to BUFSIZE)
        """ 
        #Define the socket
        self.address = Address
        self.port    = Port
        self.sock    = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        #Define the suscribers
        self.suscribers = []
        
        #And now define the steps that eveyone must have
        self.knownMessages = {}
        #The Suscribe
        self.knownMessages[SUSCRIBE_MSG] = MessageHandler(SUSCRIBE_MSG, False)
        #And the Query
        self.knownMessages[QUERY_MSG] = MessageHandler(QUERY_MSG, False)
        self.knownMessages[QUERY_MSG].makeQueryMsg(ID, Description)
        self.ID = ID
    
    def close(self):
        """
          The close function closes the socket
        """
        self.sock.close()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
    def handleMessage(self, allSteps, convSocket, dataTx):
        """
          Function used to Follow the steps of a Message handler.
          @param allSteps: Steps from Message handler
          @param convSocket: Socket Object that will be used to send or recv
          @param dataTx: Optional data meant to be used when the data of 
            SEND_TX is None
          @return: Returns a list of the Strings received from the WAIT_RX steps
        """
        dataRx = []
        dataArgIdx = 0
        for step in allSteps:
            if step['type'] == SEND_TX:
                #For transmitting Data we have two options:
                if step['data'] is None:
                    #When None we use the dataTx
                    print convSocket.send(dataTx[dataArgIdx])
                    #Then wait for the ACK
                    convSocket.recv(BUFSIZE)
                    dataArgIdx += 1
                else:
                    #otherwise we send the data as is
                    print convSocket.send(step['data'])
                    #Wait for the Ack
                    convSocket.recv(BUFSIZE)
                
            elif step['type'] == WAIT_RX:
                #When Waiting just receive the message and put it on the RX
                dataRx.extend(convSocket.recv(BUFSIZE).split(SEPARATOR))
                convSocket.send(ACK)
            else:
                raise Exception("Step Type: "  + str(step['type']) + \
                "not supported")
        return dataRx
        
    def isValidMessage(self, messageId, requestorId):
        """
          Check if teh messageId can be served
          @param messageId: ID of the message
          @param requestorId: ID of the entity requesting the message
        """
        #First check if we know the message
        if messageId in self.knownMessages:
            #Then check if we know the requestor
            return (not self.knownMessages[messageId].validate) or \
              (requestorId in self.suscribers)
            
        #If we haven't return at this point, return false
        return False
    
    def addMessage(self, newMessage):
        """
          This function adds a Message to the list of known messages
          @param newMessage: Instance of MessageHandler containing the message 
            specification
        """ 
        #First make sure that the NewMessage is of class Message handler
        if not isinstance(newMessage, MessageHandler):
            raise Exception("newMessage("  + newMessage.__class__.__name__ + \
            ") should be a MessageHandler object")
        else:
            #Now let's add it
            self.knownMessages[newMessage.MsgType] = newMessage
    
    
class DataProducer(DataAgent):
    def __init__(self, ID, Description):
        """
          The constructor instantiates the Data Producer and opens up the 
          connection.
          @param ID: Unique Identification number of the device
          @param Description: What type of Data is being produced by the device
        """ 
        super(DataProducer, self).__init__(ID, Description, '', PORT, BUFSIZE)
        self.sock.bind((self.address, self.port))
        self.sock.listen(1)
        #Initialize the actual connection to none.
        self.conversation = None
        self.currentDeviceCommType = None

    def close(self):
        """
          The close function closes all the conversations and the socket
        """
        super(DataProducer, self).close()
        if not (self.conversation is None):
            self.conversation.close()
    
    def waitConnection(self):
        """
          This function waits for a connection to happen
          @return: Returns the address of the connection
        """
        address = None
        self.conversation, address = self.sock.accept()
        self.conversation.settimeout(120.0)
        return address
        
    def checkRequest(self):
        """
          This function serves a request from a connection
          @return: Returns: 
                     - 0 when the converstion is succesfull.
                     - 1 when this command is run with no conversation is 
                       active.
                     - 2 when the request has an error
                     - 3 when the request coudln't be served
        """
        if self.conversation == None:
            return 1
        else:
            #Every conversation will start with the ID and the Communication 
            #Type(separated by Unit separator character)
            msg = self.conversation.recv(BUFSIZE).split(SEPARATOR)
            if len(msg) != 2:
                self.conversation.send(NAK)
                self.conversation.close()
                self.conversation = None
                return 2
            self.__deviceId = msg[0]
            self.currentDeviceCommType = int(msg[1])
            #Check if we can serve the Message and respond
            if not self.isValidMessage(self.currentDeviceCommType, self.__deviceId):
                #We can't serve this message, return Nack
                self.conversation.send(NAK)
                #and close the conversation
                self.conversation.close()
                self.currentDeviceCommType = None
                self.conversation = None
                return 3
            else:
                #We can serve the message, let the requestor know and exit so we 
                #can get the data the requestor wants
                self.conversation.send(ACK)
                return 0
                
    def serveRequest(self, *args):
        """
          This function serves a request from a connection
          @param *args: undefined arguments that will be used by the MessagHandler
          @return: Returns a list of the Strings received from the WAIT_RX steps 
        """
        if self.conversation == None:
            return 1
        #Let's serve this message, now we can actually use the message 
        #Handler
        retVal = []
        try:
            retVal.extend(self.handleMessage(
                allSteps   = self.knownMessages[self.currentDeviceCommType].producerSteps, 
                convSocket = self.conversation,
                dataTx     = args))
            
            #Send the final EOT and close
            self.conversation.recv(BUFSIZE)
            self.conversation.close()
            self.conversation = None
            
            #Now let's handle special messages for suscription
            if self.currentDeviceCommType == SUSCRIBE_MSG and not \
               (self.__deviceId in self.suscribers):
                #Add the suscriber to the list
                self.suscribers.append(self.__deviceId)
                #TODO: Add the suscription process for new message types
            
            self.currentDeviceCommType = None
            return retVal
        except socket.timeout:
            print('Connection from ' + str(self.__deviceId) + 'timed out')
            self.conversation.close()
            self.currentDeviceCommType = None
            self.conversation = None
            return -1
            
class DataConsumer(DataAgent):
    def __init__(self, ID, Description):
        """
          The constructor instantiates the Data Producer and opens up the 
          connection.
          @param ID: Unique Identification number of the device
          @param Description: What type of Data is being produced by the device
        """ 
        super(DataConsumer, self).__init__(ID, Description, ADDRESS, PORT, BUFSIZE)
        #For some reason, openwrt doens't allow any comm to go out trough the 
        #port unless we open first a listener.
        self.__workarround = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__workarround.bind(('', PORT))
        self.__workarround.listen(1)
        self.__workarround.settimeout(10.0)
        try:
            self.__workarround.accept()
        except socket.timeout:
            pass
        except:
            raise
        
        #Finally clean the workarround
        self.__workarround.close()
        self.__workarround = None
        
        self.__connected = False
        
    def connect(self):
        """
          This function connects to the address and port in order to send a request
        """ 
        retVal = ''
        try:
            self.sock.connect((self.address, self.port))
            self.sock.settimeout(120.0)
            self.__connected = True
        except socket.error as msg:
            self.close()
            self.__connected = False
            retVal = msg
        return retVal
    
    def request(self, CommType, *dataTx):
        """
          This function serves a request from a connection
          @param *dataTx: undefined arguments that will be used by the MessagHandler
          @return: Returns: 
                     - 0 when the converstion is succesfull.
                     - 1 when the message is not supported
                     - 2 when a connection hasn't been stablished
                     - 3 when the Producer rejects our message
        """
        retVal = {}
        retVal['Error'] = 0
        retVal['RX'] = []
        
        if not (CommType in self.knownMessages):
            retVal['Error'] = 1
            self.close()
        elif not(self.__connected):
            retVal['Error'] = 2
            self.close()
        else:
            try:
                #Every conversation will start with the ID and the Communication 
                #Type(separated by Unit separator character)
                self.sock.send(str(self.ID) + SEPARATOR + str(CommType))
                
                #The Producer will respond with either an ACK or an NAK
                resp = self.sock.recv(1)
                if resp != ACK:
                    for c in resp:
                        print ord(c)
                    retVal['Error'] = 3
                    self.close()
                else:
                    #We can now start the exchange of Data
                    retVal['RX'].append(self.handleMessage(
                        allSteps   = self.knownMessages[CommType].consumerSteps, 
                        convSocket = self.sock,
                        dataTx     = dataTx))
                    
                    #Wait for the final ACK and close
                    self.sock.send(EOT)
                    self.close()
                    
                    #Now let's handle special messages for suscription
                    if CommType == SUSCRIBE_MSG:
                        #TODO: Add the suscription process for new message types
                        pass
            except socket.timeout:
                print('Connection timed out')
                self.close()
                retVal['Error'] = TIMEOUT 
        return retVal

class wifiHandler:
    def isConnected(self, hostname = '192.168.100.1'):
        '''
          This function checks if there is a connection with the hostname (by 
          using ping).
          @param hostname: hostname to ping, defaults to 192.168.100.1
          @return: Boolean with the result of the ping operation
        '''
        response = os.system("ping -c 3 " + str(hostname))
        return (response == 0)
        
    def setCurrentWifiAsDefault(self):
        '''
          This function sets the current configuration as the default, which we 
          will revert to if anything goes wrong
        '''
        self.__defaultSsid = os.popen('uci get wireless.sta.ssid').read().rstrip()
        self.__defaultKey = os.popen('uci get wireless.sta.key').read().rstrip()
        self.__defaultEncryption = os.popen('uci get wireless.sta.encryption').read().rstrip()
        
        #Ping google.com to see if we have internet access
        self.__online = self.isConnected('www.google.com')
        
    def __init__(self, device = 'ra0'):
        self.device = device
        self.setCurrentWifiAsDefault()
    
    def __commitWifiConfig(self):
        '''
          This private function commits the wifi configuration and resets the 
          device
        '''
        os.system('uci commit')
        time.sleep(10)
        os.system('wifi')
        time.sleep(10)
    
    def scanNetworks(self):
        '''
          This function scans all the available networks
          @return: list with all the discovered networks
        '''
        print "Scanning networks, this operation may take a while"
        networks = []
        #The 7688 doesn't acquire all the SSIDs in one single seating, so we
        #have to get it 10 times and then remove duplicates
        for _ in xrange(5):
            #Get the SSIDs and parse them into a single list
            networksRaw = os.popen('iwinfo ' + str(self.device) + ' scan | grep ESSID').read()
            for networkRaw in filter(None, networksRaw.split('\n')):
                networks.append(re.search('"(.*)"', networkRaw).group(1))
        #Remove duplicates
        return list(set(networks))
    
    def switchToWifi(self, ssid, key, encryption):
        '''
          This function switches to the WIFI specified by the parameters
          @param ssid: Name of the network
          @param key: Password of the network
          @param encryption: Encryption type of the network
        '''
        os.system('uci set wireless.sta.ssid=\'' + str(ssid) + '\'')
        os.system('uci set wireless.sta.key=\'' + str(key) + '\'')
        os.system('uci set wireless.sta.encryption=\'' + str(encryption) + '\'')
        os.system('uci set wireless.sta.disabled=0')
        time.sleep(10)
        self.__commitWifiConfig()
      
    def revertWifi(self):
        '''
          This function reverts to the default wifi
        '''
        self.switchToWifi(
            ssid = self.__defaultSsid, 
            key = self.__defaultKey,
            encryption = self.__defaultEncryption)
                
    def switchApMode(self):
        os.system('uci set wireless.sta.disabled=1')
        time.sleep(10)
        self.__commitWifiConfig()
        
    def failSafe(self):
        '''
          This function is a fail safe in case something goes bad while doing any
          of the operations. If something happens we wanna revert the board to
          decent state.
          1. Try to check if we are in the default network
          2. If we aren't switch to it.
          3. Check if there is connectivity (Google will do the job)
          4. Go to AP Mode in case of no connection
        '''
        #1.
        if not (self.__defaultSsid == os.popen('uci get wireless.sta.ssid').read() and 
          self.__defaultKey == os.popen('uci get wireless.sta.key').read() and
          self.__defaultEncryption == os.popen('uci get wireless.sta.encryption').read()):
            #2.
            #Not in default config. Switch
            self.revertWifi()
            time.sleep(30)
        #3. 
        #Note that if we will check if we are supposed to be online first, if we 
        #weren't then we will assume that the connection will come eventually
        if self.__online and not (self.isConnected('google.com')):
            #4.
            self.switchApMode()
            
            