<?php
    include("index.html");
    $file = fopen("WateringSettings.txt", "w") or die("Unable to open file!");
    $line_1 = "Times " . $_POST["wateringDays"] . PHP_EOL;
    fwrite($file, $line_1);
    $i = 0;
    foreach ($_POST["weekdays"] as $value) {
        $i = $i + 1;
        $line_2 = "Day" . $i . " " . $value . PHP_EOL;
        fwrite($file, $line_2);
    }
    $line_3 = "Hour1 " . $_POST["startHour"] . PHP_EOL;
    fwrite($file, $line_3);
    $line_4 = "Min1 " . $_POST["startMinute"] . PHP_EOL;
    fwrite($file, $line_4);
    $line_5 = "Hour2 " . $_POST["endHour"] . PHP_EOL;
    fwrite($file, $line_5);
    $line_6 = "Min2 " . $_POST["endMinute"] . PHP_EOL;
    fwrite($file, $line_6);
    $line_7 = "Zip " . $_POST["zipCode"] . PHP_EOL;
    fwrite($file, $line_7);
    $line_8 = "Soil " . $_POST["soilType"] . PHP_EOL;
    fwrite($file, $line_8);
    fclose($file);
?>