import csv

f = open('Modified.log', 'w+')
with open('Merged.log', 'rb') as csvfile:
    Values = csv.reader(csvfile, quotechar='|')
    init = None
    for row in Values:
        
        #Get the string
        a = row[0].split(' ')
        #Get the day
        if init is None:
            init = int(a[0])
        day = int(a[0]) - init
        
        #Get the minutes
        b = a[1].split(':')
        minutes = (int(b[0]) * 60.0) + (int(b[1]) * 1.0) + (int(b[2]) / 60.0)
        
        f.write(str((day*24*60) + minutes) + ',' + (row[1]) + '\n')
f.close()