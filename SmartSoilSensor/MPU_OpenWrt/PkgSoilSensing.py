import time
import os
import sys
import signal
from PyMata.pymata import PyMata


def downloadMcuFirmware():
    """
      Downloads the Firmaware to the ATMEGA MCU
    """ 
    os.system('avrdude -p m32u4 -c linuxgpio -v -e -U flash:w:/root/App/MCU/FirmataPlus32u4.ino.with_bootloader.hex -U lock:w:0x0f:m')

#The next Class uses the Pymata module to access the Soil Sensor. It requires
#an anlog pin and a DIO pin that will work as the Sig and Vcc for the sensor 
#respectively
class SoilSensor:
    def __init__(self, Board, AnalogPin=0, DigPin=0):
        """
          The constructor instantiates the Sensor and it initializes it so it
          can be used.
          @param Board: Initilized PyMata Object of where the sensor is connected
          @param AnalogPin: Pin # conected to the Sig port of the sensor
          @param DigPin: Pin # connected to the Vcc Port of the sensor
        """ 
        if not isinstance(Board, PyMata):
            raise Exception("Board("  + Board.__class__.__name__ + \
            ") should be a PyMata object")
        self.SigPin       = AnalogPin
        self.VccPin       = DigPin
        self.Board        = Board
        self.AdcRawOffset = 0
        
        #Initalizing the Sensor Signal Pin and Vcc Pin
        self.Board.set_pin_mode(self.SigPin, self.Board.INPUT, self.Board.ANALOG)
        self.Board.set_pin_mode(self.VccPin, self.Board.OUTPUT, self.Board.DIGITAL)
        self.Board.enable_analog_reporting(self.SigPin)
        self.Board.enable_digital_reporting(self.VccPin)
        #And set the Vcc to 0V
        self.Board.digital_write(self.VccPin,self.Board.LOW)
        #Wait 1 second so we can read the initial Analog Value
        time.sleep(1)
        self.AdcRawOffset = self.Board.analog_read(self.SigPin)
        
        
    def readSensor(self, lowLevel = 0, highLevel = 1024):
        #In order to read the sensor we have to Turn On the Vcc
        self.Board.digital_write(self.VccPin,self.Board.HIGH)
        #Wait 1 second to let the sensor stabilize
        time.sleep(1)
        #And then just read the analog pin and scale it based on the parameters
        AiRawReading = self.Board.analog_read(self.SigPin)
        AiReading = (AiRawReading - lowLevel) / (highLevel - lowLevel)
        #Finally turn off the sensor
        self.Board.digital_write(self.VccPin,self.Board.LOW)
        return AiReading
