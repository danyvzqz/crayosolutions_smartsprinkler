import time
import sys
import signal
import os
import os.path
import logging
import traceback

sys.path.append('/root/App/MPU/')
from PyMata.pymata import PyMata
from PkgSoilSensing import *
from PkgComm import *

#Constants Definitions
ID = '0000000001'
SoilSensorSigPin = 0
SoilSensorVccPin = 0
CookiePath = '/root/App/MCU/Done.cookie'

LOW_LEVEL = 500
HIGH_LEVEL = 1000

#Enable the Logger
logging.basicConfig(
    format='%(asctime)s, %(message)s',
    filename=time.strftime("%Y_%m_%d_%H%M%SConnection.log", time.localtime()),
    level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())

try:
    #Download the firmware if it hasn't been written before
    if not os.path.isfile(CookiePath):
        logging.info("Downloading MCU Firmware")
        downloadMcuFirmware
        cookie = open(CookiePath, "w+")
        cookie.write('Done')
        cookie.close()
    
    #Initialize the Board and the sensor
    logging.info("Initializing Board and Sensor")
    board = PyMata("/dev/ttyS0", verbose=True) 
    soilSensor = SoilSensor(board, AnalogPin=SoilSensorSigPin, DigPin=SoilSensorVccPin)
    
    #Move to AP Mpde 
    logging.info("Setting Wireless")
    wifi = wifiHandler()
    #wifi.switchApMode()
    inApMode = True
    
    #Since this is the sensor, initialize the producer scheme
    logging.info("Opening Communication Ports")
    #Allow incoming port 1607
    os.system('iptables -A INPUT -j ACCEPT -p tcp --dport ' + str(PORT))
    sensorSocket = DataProducer(ID, 'Humidity Sensor (%)')
    
    #Add the GET_GENERIC_DATA_MSG
    logging.info("Adding Analog Data Message")
    sensorSocket.addMessage(MessageHandler(MsgType = GET_GENERIC_DATA_MSG, Validate = True))
    
    #Add the Toggle Network Message
    toggleMessage = MessageHandler(-1, False)
    logging.info("Adding Toggle Network Message")
    toggleMessage.addProducerStep({'type': SEND_TX, 'data': "Toggling"})
    toggleMessage.addConsumerStep(RX_STEP)
    
    sensorSocket.addMessage(toggleMessage)
    
    
    logging.info("Interrupt scheme")
    def signal_handler(sig, frame):
        print('You pressed Ctrl+C')
        if 'sensorSocket' in locals()  or 'sensorSocket' in globals():
            sensorSocket.close()
        if board is not None:
            board.close()
        sys.exit(0)
    
    signal.signal(signal.SIGINT, signal_handler)
    
    #Main
    
    #Wait for a connection
    while True:
        logging.info("Waiting For connection")
        addr = sensorSocket.waitConnection()
        logging.info("Starting Connection from: " + str(addr))
        logging.info("Checking Request")
        reqStatus = sensorSocket.checkRequest()
        if reqStatus == 0:
            logging.info("Serving Request: " + str(sensorSocket.currentDeviceCommType))
            #We only care returning Sensor Data for the Generic Data Msg
            if sensorSocket.currentDeviceCommType == GET_GENERIC_DATA_MSG:
                #When requesting the data from us, grab the sensor data and 
                #"scale" it.
                val = soilSensor.readSensor(LOW_LEVEL, HIGH_LEVEL)
                logging.info("Sensor Val: " + str(val) + "%")
                msg = sensorSocket.serveRequest(str(val))
            elif sensorSocket.currentDeviceCommType == SUSCRIBE_MSG:
                 msg = sensorSocket.serveRequest()
                 logging.info("Devices suscribed: " + str(sensorSocket.suscribers))
            elif sensorSocket.currentDeviceCommType == -1:
                msg = sensorSocket.serveRequest()
                logging.info("Toggling Network")
                if inApMode:
                    wifi.switchToWifi(ssid = 'El Gato Volador',key = 'H00tW00f',encryption = 'psk2')
                    inApMode = False
                else:
                    #wifi.switchApMode()
                    inApMode = True
            else:
                msg = sensorSocket.serveRequest()
                
            
            #Print the message
            if msg:
                logging.info("Values from Conversation: " + str(msg))
        else:
            logging.error("Request Error: " + str(reqStatus))
        
        
    logging.info("Connection Close. Bye!")
        
    #Cleanup
    board.close()
    sensorSocket.close()
except:
    logging.error("Error Occured: " + str(traceback.format_exc()))
    if 'sensorSocket' in locals()  or 'sensorSocket' in globals():
        sensorSocket.close()
    
    if 'wifi' in locals()  or 'wifi' in globals():
        logging.info("Reconnecting with El gato volador ")
        wifi.switchToWifi(ssid = 'El Gato Volador', key = 'H00tW00f', encryption = 'psk2')
    
    if board is not None:
        board.close()
        
    raise