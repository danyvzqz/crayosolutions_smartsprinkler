import time
import sys
import signal
import os
import os.path
import logging

from PyMata.pymata import PyMata
from PkgSoilSensing import *
#Constants Definitions
SoilSensorSigPin = 0
SoilSensorVccPin = 0
CookiePath = '/root/App/MCU/Done.cookie'

#Enable the Logger
'''
logging.basicConfig(
    format='%(asctime)s, %(message)s',
    filename=time.strftime("%Y_%m_%d_%H%M%S.log", time.localtime()),
    level=logging.INFO)
'''

#Download the firmware if it hasn't been written before
if not os.path.isfile(CookiePath):
    downloadMcuFirmware
    cookie = open(CookiePath, "w+")
    cookie.write('Done')
    cookie.close()

#Initialize the Board and the sensor
board = PyMata("/dev/ttyS0", verbose=True) 
soilSensor = SoilSensor(board, AnalogPin=SoilSensorSigPin, DigPin=SoilSensorVccPin)

def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    if board is not None:
        board.close()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

#Main
board.set_pin_mode(1, board.INPUT, board.ANALOG)
board.set_pin_mode(0, board.OUTPUT, board.DIGITAL)
board.set_pin_mode(1, board.OUTPUT, board.DIGITAL)
board.enable_analog_reporting(1)
board.enable_digital_reporting(SoilSensorVccPin)

#Read analog with 0V
print "Read Off"
board.digital_write(0,board.LOW)
board.digital_write(1,board.LOW)
time.sleep(5)

for i in xrange(10):
    a = board.analog_read(1)
    b = board.analog_read(0)
    print str(a) + ', ' + str(b)
    time.sleep(5)

print "Read On"
board.digital_write(0,board.HIGH)
board.digital_write(1,board.HIGH)
time.sleep(5)
for i in xrange(10):
    a = board.analog_read(1)
    b = board.analog_read(0)
    print str(a) + ', ' + str(b)
    time.sleep(5)


#And set the Vcc to 0V

print "RawOffset: " + str(soilSensor.AdcRawOffset)

while True:
    val = soilSensor.readSensor()
    print "Sensor Val: " + str(val)
    #logging.info("Sensor Val: " + str(val))
    time.sleep(5)
    
#logging.info("End Measurement")
    
    
#Cleanup
board.close()